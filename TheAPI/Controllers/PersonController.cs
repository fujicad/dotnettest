﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TheAPI.Models;
using TheAPI.Processors;

namespace TheAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {

        private PersonProcessor _processor;

        public PersonController(PersonProcessor processor)
        {
            _processor = processor;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Person> Persons()
        {
            return _processor.GetAllPersons();
        }

        [HttpGet]
        [Route("{name}")]
        public IEnumerable<Person> Persons(string name)
        {
            return _processor.GetPersons(name);
        }


        [HttpPost]
        [Route("")]
        public void AddPerson([FromBody] Person person) 
        {
            _processor.AddPerson(person);
        }

    }
}
