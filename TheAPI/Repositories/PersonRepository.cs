﻿using System.Collections.Generic;
using TheAPI.Models;

namespace TheAPI.Repositories
{
    public interface IPersonRepository
    {
        public void Save(Person name);
        public Person Find(string name);
        public IEnumerable<Person> FindAll();
    }
    public class InMemoryPersonRepository
    {
        private List<Person> _people;

        public InMemoryPersonRepository(List<Person> initPeople)
        {
            _people = initPeople;
        }

        public IEnumerable<Person> FindAll()
        {
            return _people;
        }

        public void Save(Person person)
        {
            _people.Add(person);
        }
    }
}
