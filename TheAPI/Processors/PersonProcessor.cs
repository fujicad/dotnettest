﻿using System.Collections.Generic;
using TheAPI.Models;
using TheAPI.Repositories;

namespace TheAPI.Processors
{
    public interface IPersonProcessor
    {
        public IEnumerable<Person> GetPersons(string name);
        public IEnumerable<Person> GetAllPersons();
        public void AddPerson(Person person);
    }
    public class PersonProcessor : IPersonProcessor
    {
        private IPersonRepository _repo;
        public PersonProcessor() { }
        public void AddPerson(Person person)
        {
            _repo.Save(person);
        }

        public IEnumerable<Person> GetAllPersons()
        {
            return _repo.FindAll();
        }

        public IEnumerable<Person> GetPersons(string name)
        {
            return _repo.Find(name);
        }
    }
}
