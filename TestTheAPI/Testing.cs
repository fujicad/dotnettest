﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheAPI.Models;
using TheAPI.Repositories;
using Xunit;

namespace TheAPI.tests
{
    public class Testing
    {

        [Theory]
        [InlineData("Tristan", 1)]
        [InlineData("david", 2)]
        [InlineData("SomeRandomNobody", 0)]
        [InlineData("stephen ", 1)]
        [InlineData(" stephen", 1)]
        [InlineData(" stephen ", 1)]
        [InlineData("", 0)]
        [InlineData(null, 0)]
        public void Find_ByNameAsCaseInsensitive_ReturnsExpectedPersons(string name, int expectedMatches)
        {
            //Arrange
            var testData = new List<Person>()
            {
                    new Person() { Age= 35, Name= "Tristan", Gender = "Male"},
                    new Person() { Age = 30, Name = "David", Gender = "Male" },
                    new Person() { Age = 30, Name = "Jessica", Gender = "Female" },
                    new Person() { Age = 44, Name = "david", Gender = "Male" },
                    new Person() { Age = 100000, Name = "Stephen", Gender = "Male" },
            };
            var repo = new InMemoryPersonRepository(testData);

            //Act
            var result = repo.Find(name);

            //Assert
            if (expectedMatches == 0)
            {
                Assert.Empty(result);
            }
            else
            {
                Assert.Equal(expectedMatches, result.Count());
                string actualName = name.Trim();

                foreach (var person in result)
                {
                    Assert.Equal(actualName, person.Name, ignoreCase: true);
                }
            }
        }


    }
}
